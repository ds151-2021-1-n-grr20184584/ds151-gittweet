import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const RepoTweet = ({ repo }) => {
  const navigation = useNavigation();

  const { avatarUrl, name, owner, pushedAt, description } = repo;

  const year = pushedAt.substr(0, 4);
  const month = pushedAt.substr(5, 2);
  const day = pushedAt.substr(8, 2);
  const hours = pushedAt.substr(11, 2);
  const minutes = pushedAt.substr(14, 2);
  const seconds = pushedAt.substr(17, 2);

  const pushedAtString = `${day}/${month}/${year} - ${hours}:${minutes}:${seconds} UTC`;
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => navigation.navigate('RepoInfo', { owner, name })}
    >
      <View style={styles.innerContainer}>
        <Image style={styles.avatar} source={{ uri: avatarUrl }} />
        <View style={styles.content}>
          <Text style={styles.title}>{name}</Text>
          <View style={styles.about}>
            <Text style={styles.owner}>{owner}</Text>
            <Text style={styles.time}>{pushedAtString}</Text>
          </View>
          <Text style={styles.description}>
            {description?.length > 90
              ? description.substr(0, 90) + ' [...]'
              : description}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    borderWidth: 0.5,
    borderColor: 'darkgray',
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
  innerContainer: {
    flexDirection: 'row',
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    flex: 1,
  },
  content: {
    flex: 6,
    paddingHorizontal: 12,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  about: {
    flexDirection: 'row',
  },
  owner: {
    fontSize: 11,
    flex: 1,
    textAlign: 'left',
  },
  time: {
    fontSize: 11,
    color: 'darkgray',
    flex: 1,
    textAlign: 'right',
  },
  description: {},
});

export default RepoTweet;
