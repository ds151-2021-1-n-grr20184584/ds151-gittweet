import React, { useContext } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItem,
  DrawerItemList,
} from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { FontAwesome } from '@expo/vector-icons';
import { AuthContext } from '../contexts/AuthContext';
import LoginScreen from '../screens/LoginScreen';
import FollowingReposScreen from '../screens/FollowingReposScreen';
import UserReposScreen from '../screens/UserReposScreen';
import RepoInfoScreen from '../screens/RepoInfoScreen';

const Drawer = createDrawerNavigator();
const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const TabScreens = () => {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color, size }) => {
          const iconName = route.name === 'FollowingRepos' ? 'users' : 'user';
          return (
            <FontAwesome name={iconName} size={size * 0.8} color={color} />
          );
        },
        tabBarLabel:
          route.name === 'FollowingRepos' ? "Following's Repos" : 'My Repos',
        headerShown: false,
      })}
    >
      <Tab.Screen name="FollowingRepos" component={FollowingReposScreen} />
      <Tab.Screen name="MyRepos" component={UserReposScreen} />
    </Tab.Navigator>
  );
};

const StackScreens = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Tabs" component={TabScreens} />
      <Stack.Screen name="RepoInfo" component={RepoInfoScreen} />
    </Stack.Navigator>
  );
};

const NavigationIfLogged = () => {
  const authProvider = useContext(AuthContext);

  return authProvider.authState.access_token ? (
    <NavigationContainer>
      <Drawer.Navigator
        drawerContent={(props) => {
          return (
            <DrawerContentScrollView {...props}>
              <DrawerItemList {...props} />
              <DrawerItem
                label="Logout"
                onPress={() => authProvider.signOut()}
              />
            </DrawerContentScrollView>
          );
        }}
      >
        <Drawer.Screen
          name="Stacks"
          component={StackScreens}
          options={{ title: 'GitTweet' }}
        />
      </Drawer.Navigator>
    </NavigationContainer>
  ) : (
    <LoginScreen />
  );
};

export default NavigationIfLogged;
