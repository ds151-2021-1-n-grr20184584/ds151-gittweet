import React, { createContext, useEffect, useReducer } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { TOKEN } from '@env';

const AuthContext = createContext(null);

const authReducer = (state, action) => {
  switch (action.type) {
    case 'signIn':
      return {
        ...state,
        access_token: action.payload,
        personal_token: TOKEN,
      };
    case 'signOut':
      return { ...state, access_token: null, personal_token: null };
    default:
      return { ...state };
  }
};

const AuthProvider = ({ children }) => {
  const [authState, dispatch] = useReducer(authReducer, {
    access_token: null,
    personal_token: null,
  });

  const signIn = async (access_token) => {
    await AsyncStorage.setItem('access_token', access_token);
    await AsyncStorage.setItem('personal_token', TOKEN);
    dispatch({ type: 'signIn', payload: access_token });
  };

  const signOut = async () => {
    await AsyncStorage.removeItem('access_token');
    await AsyncStorage.removeItem('personal_token');
    dispatch({ type: 'signOut', payload: null });
  };

  useEffect(async () => {
    const access_token = await AsyncStorage.getItem('access_token');
    if (access_token) {
      await signIn(access_token);
    }
  }, []);

  return (
    <AuthContext.Provider value={{ authState, signIn, signOut }}>
      {children}
    </AuthContext.Provider>
  );
};

export { AuthProvider, AuthContext };
