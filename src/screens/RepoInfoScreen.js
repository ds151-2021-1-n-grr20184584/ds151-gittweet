import React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import { useQuery } from '@apollo/client';
import { FontAwesome } from '@expo/vector-icons';
import { GET_REPO } from '../api/github-graphql-queries';

const RepoInfoScreen = ({ route }) => {
  const { owner, name } = route.params;
  const { loading, error, data } = useQuery(GET_REPO, {
    variables: { owner, name },
  });

  if (loading) return <Text>Loading...</Text>;
  if (error) return <Text>{`Error! ${error}`}</Text>;

  const aux = data.repository;

  const repo = {
    description: aux.description,
    isArchived: aux.isArchived,
    isFork: aux.isFork,
    isPrivate: aux.isPrivate,
    name: aux.name,
    owner: aux.owner.login,
    avatarUrl: aux.owner.avatarUrl,
    watchers: aux.watchers.totalCount,
    stars: aux.stargazerCount,
    forks: aux.forkCount,
    parent: {
      name: aux.parent?.name,
      owner: aux.parent?.owner?.login,
      avatarUrl: aux.parent?.owner?.avatarUrl,
    },
    primaryLanguage: {
      name: aux.primaryLanguage.name,
      color: aux.primaryLanguage.color,
    },
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        {repo.isArchived ? (
          <View style={styles.archivedContainer}>
            <Text style={styles.archivedText}>Archived</Text>
          </View>
        ) : (
          <></>
        )}
        <View style={styles.innerHeader}>
          <Image style={styles.ownerImage} source={{ uri: repo.avatarUrl }} />
          <View style={styles.headerInfo}>
            <View>
              <Text style={styles.title}>{repo.name}</Text>
              <Text style={styles.owner}>{repo.owner}</Text>
            </View>
            {repo.isFork ? (
              <View style={styles.forkInfo}>
                <Image
                  style={styles.forkImage}
                  source={{ uri: repo.parent.avatarUrl }}
                />
                <Text style={styles.forkText}>
                  Forked from:{' '}
                  <Text style={{ ...styles.forkText, ...styles.forkUnderline }}>
                    {repo.parent.owner}/{repo.parent.name}
                  </Text>
                </Text>
              </View>
            ) : (
              <></>
            )}
          </View>
        </View>
      </View>
      <View style={styles.descriptionContainer}>
        <Text style={styles.description}>{repo.description}</Text>
      </View>
      <View style={styles.socialContainer}>
        <View style={styles.socialItem}>
          <View style={styles.socialIconWrapper}>
            <FontAwesome
              style={styles.socialIcon}
              name="eye"
              size={28}
              color="darkgray"
            />
          </View>
          <Text style={styles.socialCount}> {repo.watchers}</Text>
        </View>
        <View style={styles.socialItem}>
          <View style={styles.socialIconWrapper}>
            <FontAwesome
              style={styles.socialIcon}
              name="star-o"
              size={28}
              color="darkgray"
            />
          </View>
          <Text style={styles.socialCount}> {repo.stars}</Text>
        </View>
        <View style={styles.socialItem}>
          <View style={styles.socialIconWrapper}>
            <FontAwesome
              style={styles.socialIcon}
              name="code-fork"
              size={28}
              color="darkgray"
            />
          </View>
          <Text style={styles.socialCount}> {repo.forks}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  archivedContainer: {
    flex: 1,
    backgroundColor: '#6e5494',
    borderBottomWidth: 1,
    borderColor: '#4a3334',
    borderRadius: 2,
  },
  archivedText: {
    color: '#4a3334',
    fontWeight: 'bold',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  headerContainer: {
    flex: 5,
    borderBottomWidth: 1,
    borderColor: 'lightgray',
  },
  innerHeader: {
    flexDirection: 'row',
    padding: 10,
  },
  ownerImage: {
    width: 80,
    height: 80,
    borderRadius: 50,
  },
  headerInfo: {
    marginLeft: 4,
    justifyContent: 'space-between',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 24,
  },
  owner: {
    fontSize: 14,
  },
  forkInfo: {
    flexDirection: 'row',
  },
  forkImage: {
    width: 15,
    height: 15,
    borderRadius: 50,
  },
  forkText: {
    fontSize: 11,
  },
  forkUnderline: {
    textDecorationLine: 'underline',
  },
  descriptionContainer: {
    flex: 18,
    padding: 10,
  },
  description: {
    fontSize: 16,
  },
  socialContainer: {
    flex: 2,
    borderTopWidth: 1,
    borderColor: 'lightgray',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    paddingVertical: 10,
  },
  socialItem: {
    flexDirection: 'row',
    borderWidth: 2,
    borderColor: 'darkgray',
    borderRadius: 15,
    paddingHorizontal: 10,
    paddingVertical: 5,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  socialIconWrapper: {
    marginHorizontal: 4,
    borderRightWidth: 2,
    borderRightColor: 'darkgray',
  },
  socialIcon: {
    marginRight: 8,
  },
  socialCount: {
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 5,
    marginRight: 10,
    textAlign: 'center',
    color: 'darkgray',
  },
});

export default RepoInfoScreen;
