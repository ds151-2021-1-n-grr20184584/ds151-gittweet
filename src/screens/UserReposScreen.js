import React from 'react';
import { View, FlatList, Text } from 'react-native';
import { useQuery } from '@apollo/client';
import { GET_USER_REPOS } from '../api/github-graphql-queries';
import RepoTweet from '../components/RepoTweet';

const UserReposScreen = () => {
  const { loading, error, data } = useQuery(GET_USER_REPOS);

  if (loading) return <Text>Loading...</Text>;
  if (error) return <Text>{`Error! ${error}`}</Text>;

  const repos = data.viewer.repositories.nodes.map((r) => ({
    name: r.name,
    pushedAt: r.pushedAt,
    description: r.description,
    owner: r.owner.login,
    avatarUrl: r.owner.avatarUrl,
  }));

  return (
    <View>
      <FlatList
        data={repos}
        keyExtractor={(r) => `${r.owner}:${r.name}:${r.pushedAt}`}
        renderItem={({ item: repo }) => <RepoTweet repo={repo} />}
      />
    </View>
  );
};

export default UserReposScreen;
