import React from 'react';
import { FlatList, Text, View } from 'react-native';
import { useQuery } from '@apollo/client';
import { GET_FOLLOWED_USERS_REPOS } from '../api/github-graphql-queries';
import RepoTweet from '../components/RepoTweet';

const FollowingReposScreen = () => {
  const { loading, error, data } = useQuery(GET_FOLLOWED_USERS_REPOS);

  if (loading) return <Text>Loading...</Text>;
  if (error) return <Text>{`Error! ${error}`}</Text>;

  const repos = Array.from(
    new Set(
      data.viewer.following.nodes
        .map((n) => n.repositories.nodes)
        .flat()
        .map((r) => ({
          name: r.name,
          pushedAt: r.pushedAt,
          description: r.description,
          owner: r.owner.login,
          avatarUrl: r.owner.avatarUrl,
        }))
        .sort((r1, r2) => r2.pushedAt > r1.pushedAt),
    ),
  );

  return (
    <View>
      <FlatList
        data={repos}
        keyExtractor={(r) => `${r.owner}:${r.name}:${r.pushedAt}`}
        renderItem={({ item: repo }) => <RepoTweet repo={repo} />}
      />
    </View>
  );
};

export default FollowingReposScreen;
