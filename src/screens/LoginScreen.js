import React, { useContext, useEffect } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { makeRedirectUri, useAuthRequest } from 'expo-auth-session';
import { FontAwesome } from '@expo/vector-icons';
import axios from 'axios';
import { CLIENT_ID, CLIENT_SECRET } from '@env';
import { AuthContext } from '../contexts/AuthContext';
import * as WebBrowser from 'expo-web-browser';

WebBrowser.maybeCompleteAuthSession();

const discovery = {
  authorizationEndpoint: 'https://github.com/login/oauth/authorize',
  tokenEndpoint: 'https://github.com/login/oauth/access_token',
  revocationEndpoint: `https://github.com/settings/connections/applications/${CLIENT_ID}`,
};

const LoginScreen = () => {
  const authContext = useContext(AuthContext);

  const [request, response, promptAsync] = useAuthRequest(
    {
      clientId: CLIENT_ID,
      scopes: [
        'identity',
        'user',
        'repo',
        'public_repo',
        'repo_deployment',
        'repo:status',
        'read:repo_hook',
        'read:org',
        'read:public_key',
        'read:gpg_key',
      ],
      redirectUri: makeRedirectUri({
        scheme: 'dev-losername-gittweet-github-app',
      }),
    },
    discovery,
  );

  useEffect(async () => {
    if (response?.type === 'success') {
      const { code } = response.params;
      const newResponse = await axios.post(
        'https://github.com/login/oauth/access_token',
        {
          client_id: CLIENT_ID,
          client_secret: CLIENT_SECRET,
          code,
        },
        {
          headers: {
            Accept: 'application/json',
          },
        },
      );

      const access_token = newResponse.data.access_token;

      await authContext.signIn(access_token);
    }
  }, [response]);

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.button}
        disabled={!request}
        onPress={() => {
          promptAsync();
        }}
      >
        <FontAwesome
          style={styles.buttonIcon}
          name="github"
          size={32}
          color="#fafafa"
        />
        <Text style={styles.buttonText}>Login with GitHub</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  button: {
    backgroundColor: '#333',
    flexDirection: 'row',
    paddingVertical: 14,
    textAlign: 'center',
    justifyContent: 'center',
    alignContent: 'center',
    margin: 10,
    borderRadius: 8,
  },
  buttonIcon: {
    paddingHorizontal: 4,
  },
  buttonText: {
    paddingHorizontal: 9,
    fontSize: 18,
    color: '#fafafa',
    textAlignVertical: 'center',
    lineHeight: 32,
    fontWeight: 'bold',
  },
});

export default LoginScreen;
