import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import AsyncStorage from '@react-native-async-storage/async-storage';

const httpLink = createHttpLink({
  uri: 'https://api.github.com/graphql',
});

const authLink = setContext(async (_, { headers }) => {
  const access_token = await AsyncStorage.getItem('access_token');

  return {
    headers: {
      ...headers,
      Authorization: `Bearer ${access_token}`,
    },
  };
});

const githubClient = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
});

export default githubClient;
