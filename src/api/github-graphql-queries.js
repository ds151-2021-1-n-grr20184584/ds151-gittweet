import { gql } from '@apollo/client';

const GET_FOLLOWED_USERS_REPOS = gql`
  query GetFollowedUsersRepos {
    viewer {
      following(first: 100) {
        nodes {
          repositories(
            orderBy: { field: PUSHED_AT, direction: DESC }
            first: 100
          ) {
            nodes {
              name
              pushedAt
              description
              owner {
                login
                avatarUrl
              }
            }
          }
        }
      }
    }
  }
`;

const GET_USER_REPOS = gql`
  query GetUserRepos {
    viewer {
      repositories(orderBy: { field: PUSHED_AT, direction: DESC }, first: 100) {
        nodes {
          name
          pushedAt
          description
          owner {
            login
            avatarUrl
          }
        }
      }
    }
  }
`;

const GET_REPO = gql`
  query GetRepo($name: String!, $owner: String!) {
    repository(name: $name, owner: $owner) {
      isFork
      name
      description
      isArchived
      isPrivate
      forkCount
      stargazerCount
      watchers {
        totalCount
      }
      primaryLanguage {
        name
        color
      }
      owner {
        login
        avatarUrl
      }
      parent {
        name
        owner {
          login
          avatarUrl
        }
      }
    }
  }
`;

export { GET_FOLLOWED_USERS_REPOS, GET_USER_REPOS, GET_REPO };
