import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const github = axios.create({
  baseURL: 'https://api.github.com',
});

github.interceptors.request.use(async (config) => {
  const access_token = await AsyncStorage.getItem('access_token');
  if (access_token) {
    config.headers.Authorization = `Bearer ${access_token}`;
  }
  return config;
});

const getFollowing = async () => {
  return (await github.get('/user/following')).data;
};

const getReposForUser = async (user) => {
  return (
    await github.get(`/users/${user}/repos`, {
      params: { sort: 'updated' },
    })
  ).data;
};

const getFollowedUsersRepos = async () => {
  const users = await getFollowing();

  const promises = users.map(async (user) => {
    let repos = await getReposForUser(user.login);
    return repos;
  });

  const repos = (await Promise.all(promises)).flat().sort((r) => r.pushed_at);
  return repos;
};

export default github;

export { getFollowing, getReposForUser, getFollowedUsersRepos };
