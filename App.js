import React from 'react';
import { ApolloProvider } from '@apollo/client';
import { AuthProvider } from './src/contexts/AuthContext';
import NavigationIfLogged from './src/components/navigation';
import githubClient from './src/api/github-graphql-client';

const App = () => {
  return (
    <AuthProvider>
      <ApolloProvider client={githubClient}>
        <NavigationIfLogged />
      </ApolloProvider>
    </AuthProvider>
  );
};

export default App;
